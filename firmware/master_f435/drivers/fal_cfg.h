/*
 * Copyright (c) 2006-2021, RT-Thread Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2023-09-04     SenyPC       the first version
 */
#ifndef DRIVERS_FAL_CFG_H_
#define DRIVERS_FAL_CFG_H_

#include <rtconfig.h>
#include <board.h>

#define NOR_FLASH_DEV_NAME             "norflash0"
#define SP_REGS_NAME                    "sp_regs"
#define BLK_DEV_NAME                    "filesystem"

/* ===================== Flash device Configuration ========================= */
extern const struct fal_flash_dev at32_onchip_flash;
extern struct fal_flash_dev nor_flash0;

/* flash device table */
#define FAL_FLASH_DEV_TABLE                                          \
{                                                                    \
    &at32_onchip_flash,                                           \
    &nor_flash0,                                                     \
}
/* ====================== Partition Configuration ========================== */
#ifdef FAL_PART_HAS_TABLE_CFG
/* partition table */
#define FAL_PART_TABLE                                                               \
{                                                                                    \
    {FAL_PART_MAGIC_WORD,   "bl",           "onchip_flash",         0,              96*1024,        0}, \
    {FAL_PART_MAGIC_WORD,   "app",          "onchip_flash",         96*1024,        1964*1024,      0}, \
    {FAL_PART_MAGIC_WORD,   "update",       "onchip_flash",         2060*1024,      1964*1024,      0}, \
    {FAL_PART_MAGIC_WORD,   SP_REGS_NAME,   "onchip_flash",         4024*1024,      4*1024,         0}, \
    {FAL_PART_MAGIC_WORD,   "dev_info",     "onchip_flash",         4028*1024,      4*1024,         0}, \
    {FAL_PART_MAGIC_WORD,   BLK_DEV_NAME,   NOR_FLASH_DEV_NAME,     0,              8*1024*1024,    0}, \
}
#endif /* FAL_PART_HAS_TABLE_CFG */

#endif /* DRIVERS_FAL_CFG_H_ */
