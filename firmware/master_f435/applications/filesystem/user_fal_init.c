/*
 * Copyright (c) 2006-2021, RT-Thread Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2023-09-04     SenyPC       the first version
 */
#include <rtthread.h>
#include "fal.h"

static int user_fal_init(void) {
    fal_init();
    fal_mtd_nor_device_create(BLK_DEV_NAME);
    return RT_EOK;
}
INIT_COMPONENT_EXPORT(user_fal_init);
