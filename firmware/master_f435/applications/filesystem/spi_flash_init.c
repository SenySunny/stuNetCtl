/*
 * Copyright (c) 2006-2021, RT-Thread Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2023-09-04     SenyPC       the first version
 */
#include <rtthread.h>
#include <board.h>
#include <rtdevice.h>
#include "spi_flash.h"
#include "spi_flash_sfud.h"
#include "drv_spi.h"

static int rt_hw_spi_flash_init(void)
{
    crm_periph_clock_enable(CRM_GPIOB_PERIPH_CLOCK, TRUE);
    rt_hw_spi_device_attach("spi2", "spi20", GPIOE, GPIO_PINS_15);
    if (RT_NULL == rt_sfud_flash_probe("norflash0", "spi20"))  //注册块设备，这一步可以将外部flash抽象为系统的块设备
    {
        return -RT_ERROR;
    }
    return RT_EOK;
}
/* 导出到自动初始化,组件初始化 */
INIT_DEVICE_EXPORT(rt_hw_spi_flash_init);
