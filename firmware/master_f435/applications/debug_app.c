/*
 * Copyright (c) 2006-2021, RT-Thread Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2023-09-04     SenyPC       the first version
 */
#include <rtthread.h>

#define DBG_TAG "app.debug"
#define DBG_LVL DBG_INFO
#include <rtdbg.h>

#define     DEBUG_MODE          1

#define     DEBUG_DEVICE        "uart1"

//定义开启和关闭打印数据，为了实现可以再运行中关闭和打开。
//只需要把dbg_mode指向一个可以远程修改的变量，就可以通过外部命令关闭和打开调试输出
int dbg_mode_status = DEBUG_MODE;
int *dbg_mode = &dbg_mode_status;


/**
 * @brief This function will print a formatted string on system console.
 *
 * @param fmt is the format parameters.
 *
 * @return The number of characters actually written to buffer.
 */
int rt_kprintf(const char *fmt, ...)
{
    rt_size_t length = 0;
    if((*dbg_mode) > 0)
    {
        va_list args;
        static char rt_log_buf[RT_CONSOLEBUF_SIZE];

        va_start(args, fmt);

        length = rt_vsnprintf(rt_log_buf, sizeof(rt_log_buf) - 1, fmt, args);
        if (length > RT_CONSOLEBUF_SIZE - 1)
            length = RT_CONSOLEBUF_SIZE - 1;
        if((*dbg_mode) == 1)
        {
            rt_device_t _console_device = rt_console_get_device();
            if (_console_device != RT_NULL)
            {
                rt_device_write(_console_device, 0, rt_log_buf, length);
            }
        }
        else
        {

        }
        va_end(args);
    }
    return length;
}
