/*
 * Copyright (c) 2006-2021, RT-Thread Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2023-08-29     SenyPC       the first version
 */
#ifndef APPLICATIONS_RTT_UART_RTT_UART_H_
#define APPLICATIONS_RTT_UART_RTT_UART_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "modbus_rt_platform_thread.h"

#include <rtdevice.h>
#include <drv_usart.h>

typedef struct at32_uart  rtt_usart;

//内部调试串口
#define RTU_USING_UART1
//#define RTU_UART1_USING_RS485
//DB9 RS485接口
#define RTU_USING_UART2
//#define RTU_UART2_USING_RS485
//IO扩展串口
#define RTU_USING_UART3
//#define RTU_UART3_USING_RS485
//DB9 RS232接口
#define RTU_USING_UART6
//#define RTU_UART6_USING_RS485



//IO 序号参考drv_gpio.c的定义
#if defined(RTU_USING_UART1) && defined(RTU_UART1_USING_RS485)
#define UART1_RS485_RE_PIN          8                //PA8
#define uart1_rs485_tx_en()         rt_pin_write(UART1_RS485_RE_PIN, PIN_HIGH)
#define uart1_rs485_rx_en()         rt_pin_write(UART1_RS485_RE_PIN, PIN_LOW)
#endif

#if defined(RTU_USING_UART2) && defined(RTU_UART2_USING_RS485)
#define UART2_RS485_RE_PIN          69                //PE5
#define uart2_rs485_tx_en()         rt_pin_write(UART2_RS485_RE_PIN, PIN_HIGH)
#define uart2_rs485_rx_en()         rt_pin_write(UART2_RS485_RE_PIN, PIN_LOW)
#endif

#if defined(RTU_USING_UART3) && defined(RTU_UART3_USING_RS485)
#define UART3_RS485_RE_PIN          51                //PD3
#define uart3_rs485_tx_en()         rt_pin_write(UART3_RS485_RE_PIN, PIN_HIGH)
#define uart3_rs485_rx_en()         rt_pin_write(UART3_RS485_RE_PIN, PIN_LOW)
#endif

#if defined(RTU_USING_UART4) && defined(RTU_UART4_USING_RS485)
#define UART4_RS485_RE_PIN          20                //PC3
#define uart4_rs485_tx_en()         rt_pin_write(UART4_RS485_RE_PIN, PIN_HIGH)
#define uart4_rs485_rx_en()         rt_pin_write(UART4_RS485_RE_PIN, PIN_LOW)
#endif

#if defined(RTU_USING_UART5) && defined(RTU_UART5_USING_RS485)
#define UART5_RS485_RE_PIN          52                //PD4
#define uart5_rs485_tx_en()         rt_pin_write(UART5_RS485_RE_PIN, PIN_HIGH)
#define uart5_rs485_rx_en()         rt_pin_write(UART5_RS485_RE_PIN, PIN_LOW)
#endif

#if defined(RTU_USING_UART6) && defined(RTU_UART6_USING_RS485)
#define UART6_RS485_RE_PIN          20                //PC3
#define uart6_rs485_tx_en()         rt_pin_write(UART6_RS485_RE_PIN, PIN_HIGH)
#define uart6_rs485_rx_en()         rt_pin_write(UART6_RS485_RE_PIN, PIN_LOW)
#endif

#if defined(RTU_USING_UART7) && defined(RTU_UART7_USING_RS485)
#define UART7_RS485_RE_PIN          20                //PC3
#define uart7_rs485_tx_en()         rt_pin_write(UART7_RS485_RE_PIN, PIN_HIGH)
#define uart7_rs485_rx_en()         rt_pin_write(UART7_RS485_RE_PIN, PIN_LOW)
#endif

#if defined(RTU_USING_UART8) && defined(RTU_UART8_USING_RS485)
#define UART8_RS485_RE_PIN          20                //PC3
#define uart8_rs485_tx_en()         rt_pin_write(UART8_RS485_RE_PIN, PIN_HIGH)
#define uart8_rs485_rx_en()         rt_pin_write(UART8_RS485_RE_PIN, PIN_LOW)
#endif


#ifdef RTU_USING_UART1
    extern modbus_rt_sem_t _rx1_notice;
#endif
#ifdef RTU_USING_UART2
    extern modbus_rt_sem_t _rx2_notice;
#endif
#ifdef RTU_USING_UART3
    extern modbus_rt_sem_t _rx3_notice;
#endif
#ifdef RTU_USING_UART4
    extern modbus_rt_sem_t _rx4_notice;
#endif
#ifdef RTU_USING_UART5
    extern modbus_rt_sem_t _rx5_notice;
#endif
#ifdef RTU_USING_UART6
    extern modbus_rt_sem_t _rx6_notice;
#endif
#ifdef RTU_USING_UART7
    extern modbus_rt_sem_t _rx7_notice;
#endif
#ifdef RTU_USING_UART8
    extern modbus_rt_sem_t _rx8_notice;
#endif


typedef enum {
    DIR_RX = 0,         //RS485接收模式
    DIR_TX = 1,         //RS485阻塞发送模式: 遇到串口非就绪->等待串口就绪
}rs485_dir_type;

extern rt_device_t rtt_uart_open(const char *devname, int baudrate, int bytesize, char parity, int stopbits, int xonxoff);
extern int rtt_uart_close(rt_device_t serial);
extern int rtt_uart_send(rt_device_t serial, uint8_t *buf, int len);
extern int rtt_uart_receive(rt_device_t serial, uint8_t *buf, int bufsz, int timeout, int bytes_timeout);


#ifdef __cplusplus
}
#endif

#endif /* APPLICATIONS_RTT_UART_RTT_UART_H_ */
