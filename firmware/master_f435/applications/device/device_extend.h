/*
 * Copyright (c) 2006-2021, RT-Thread Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2023-12-27     SenyPC       the first version
 */
#ifndef __DEVICE_EXTEND_H_
#define __DEVICE_EXTEND_H_

#ifdef __cplusplus
 extern "C" {
#endif

#include "drv_common.h"
#include "device_config.h"
#include "modbus_rtu.h"

#define     DEVICE_IRQ_TIME             (100)       //IRQ定时器相应时间
#define     DEVICE_IRQ_TIMEOUT          (1000)      //IRQ超时时间

#define    SERIAL_EXT_NAME              "uart3"


#define     DEVICE_EXT_BAUD             BAUD_RATE_460800

#define     DEVICE_BYTESIZE             8
#define     DEVICE_PARITY               'N'
#define     DEVICE_STOPBITS             1

/**
 * @brief   扩展设备的基本信息，存储已知的扩展设备信息
 */
struct device_extend_config {
     uint16_t type;          //设备类型

     int di_nums;           //该类型设备拥有 的di数量，这里指的是setting_nums
     int do_nums;           //该类型设备拥有 的do数量，这里指的是setting_nums

     struct device_extend_config *pre;
     struct device_extend_config *next;

 };
typedef struct device_extend_config * device_extend_config_t;

/**
 * @brief   扩展设备列表，存储第一次读取到的外设列表，
 *                      仅包含设备类型和在硬件板卡中的顺序，从1开始
 *
 */
struct device_extend_list {
    uint16_t type;          //设备类型
    int serial;              //设备地址

//    void * data;           //外设模块的数据内容，不同的外设数据内容不一致，指向对应的额结构体

    struct device_extend_list * pre;
    struct device_extend_list *next;
} ;
typedef struct device_extend_list * device_extend_list_t;


/**
 * @brief   扩展设备接口信息
 */
struct device_extend_type {
    device_pin_num  ctl_en_pin;             /* 控制信号使能  */
    /**
      * ext_cs,ext_clk控制信号使能功能：
      * 高电平时，信号关闭，总线处于高阻，
      * 低电平时，使能控制信号，所以总线上同一时刻只允许一路使能
      */
    device_pin_num  ext_cs_pin;             /* 控制信号  */
    device_pin_num  ext_clk_pin;            /* 控制信号时钟  */
    device_pin_num  ext_rdy_pin;            /* 告知下级设备，为0表示该设备已经就绪。  */

    device_pin_num  ext_dat_pin;            /* 控制数据信号  */
    device_pin_num  ext_irq_pin;            /* 下级反馈信号，低电平表示下级设备已经就绪  */
    device_pin_num  ext_chk_pin;            /* 检测是否存在下级设备信号，为1表示总线已经连接下级设备  */

    int status;                             //扩展模块扫描状态：0：未扫描完毕；1：扫描完毕
    rtu_modbus_device_t dev_ext;
    int dev_nums;
    device_extend_list_t dev_list;

    rt_sem_t sem_irq;
    rt_timer_t irq_timer;
    int     irq_timeout;                    /* 等待下级反馈信号超时  */
};
typedef struct device_extend_type * device_extend_type_t;

/**
 * @brief   扩展设备数据信息，包括设备类型，设备485地址。
 *                      设备外设数量（包括di,do,.......）
 */
struct device_extend_data {
    uint16_t type;          //设备类型
    int addr;

    int di_nums;
    int do_nums;

    struct device_extend_data *pre;
    struct device_extend_data *next;
 };
typedef struct device_extend_data * device_extend_data_t;

/**
 * @brief   扩展设备总记录,记录设备的di,do,......外设的总数量，
 *                      以及外设模块的总数量。
 */
struct device_extend_dev {
    rtu_modbus_device_t com_dev;
    int di_nums;        //记录总di的数量
    int do_nums;        //记录总do的数量

    int dev_nums;       //记录设备数量

    device_extend_data_t data_list;
};
typedef struct device_extend_dev * device_extend_dev_t;


extern int _ext_config_nums;
extern device_extend_config_t _ext_config;
extern struct device_extend_dev _ext_data;

int dev_ext_start( void );
int dev_ext_restart( void );

int dev_ext_get_status( void );

#ifdef __cplusplus
}
#endif


#endif /* APPLICATIONS_DEVICE_DEVICE_EXTEND_H_ */
