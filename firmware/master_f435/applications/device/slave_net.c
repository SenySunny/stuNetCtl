/*
 * Copyright (c) 2006-2021, RT-Thread Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2023-09-28     SenyPC       the first version
 */
#include "slave_net.h"

#define DBG_TAG "slave_net"
#define DBG_LVL DBG_LOG
#include <rtdbg.h>

tcp_modbus_device_t _tsu = NULL;
tcp_modbus_device_t _tst = NULL;

int dev_slave_udp_init( void ) {
    int ret = 0;
    _tsu = modbus_tcp(MODBUS_SLAVE);
    if(NULL == _tsu) {
        LOG_E("dev_udp create error.");
        return -MODBUS_RT_ERROR;
    }
    if(MODBUS_RT_EOK != (ret = modbus_tcp_set_net(_tsu, NULL, NET_PORT_INIT_UDP, SOCK_DGRAM))) {
        LOG_E("dev_udp set net error, code is: %d.", ret);
        return ret;
    }
    LOG_D("dev_udp: port:%d. create slave.", NET_PORT_INIT_UDP);
    if(MODBUS_RT_EOK != (ret = modbus_tcp_set_dev_binding(_tsu, 1))) {
        rt_kprintf("dev_udp set_dev_binding error, code is: %d.", ret);
        return -MODBUS_RT_ERROR;
    }
    LOG_D("dev_udp set dev_binding:%d.", 1);
    if(MODBUS_RT_EOK != (ret = modbus_tcp_set_strict(_tsu, 0))) {
        rt_kprintf("dev_udp set_strict error, code is: %d.", ret);
        return -MODBUS_RT_ERROR;
    }
    LOG_D("dev_udp set strict:%d.", 0);
    if(MODBUS_RT_EOK != (ret = modbus_tcp_set_p2p_flag(_tsu, 1))) {
        LOG_E("dev_udp set p2p flag error, code is: %d.", ret);
        return ret;
    }
    LOG_D("dev_udp set p2p flag:%d.", 1);
    if(MODBUS_RT_EOK != (ret = modbus_tcp_open(_tsu))) {
        LOG_E("dev_udp open error, code is: %d.", ret);
        return ret;
    }
    LOG_D("dev_udp open sucess.");
    return 0;
}

int dev_slave_tcp_init( void ) {
    int ret = 0;
    _tst = modbus_tcp(MODBUS_SLAVE);
    if(NULL == _tst) {
        LOG_E("dev_tcp create error.");
        return -MODBUS_RT_ERROR;
    }
    if(MODBUS_RT_EOK != (ret = modbus_tcp_set_net(_tst, NULL, NET_PORT_INIT_TCP, SOCK_STREAM))) {
        LOG_E("dev_tcp set net error, code is: %d.", ret);
        return ret;
    }
    LOG_D("dev_tcp: port:%d. create slave.", NET_PORT_INIT_TCP);
    if(MODBUS_RT_EOK != (ret = modbus_tcp_set_dev_binding(_tst, 1))) {
        rt_kprintf("dev_tcp set_dev_binding error, code is: %d.", ret);
        return -MODBUS_RT_ERROR;
    }
    LOG_D("dev_tcp set dev_binding:%d.", 1);
    if(MODBUS_RT_EOK != (ret = modbus_tcp_set_strict(_tst, 0))) {
        rt_kprintf("dev_tcp set_strict error, code is: %d.", ret);
        return -MODBUS_RT_ERROR;
    }
    LOG_D("dev_tcp set strict:%d.", 0);
    if(MODBUS_RT_EOK != (ret = modbus_tcp_open(_tst))) {
        LOG_E("dev_tcp open error, code is: %d.", ret);
        return ret;
    }
    LOG_D("dev_tcp open sucess.");
    return 0;
}

int dev_slave_init( void ) {
    int ret = MODBUS_RT_EOK;
    if(MODBUS_RT_EOK != (ret = dev_slave_udp_init())) {
        return ret;
    }
    if(MODBUS_RT_EOK != (ret = dev_slave_tcp_init())) {
        return ret;
    }
    return ret;
}

int dev_slave_net_clear( void ) {
    int ret = MODBUS_RT_EOK;
    if(NULL != _tsu) {
        ret = modbus_tcp_destroy(&_tsu);
        if(MODBUS_RT_EOK != ret) {
            LOG_E("modbus_tcp_destroy for dev_udp error,code:%d.", ret);
            return ret;
        }
        LOG_D("modbus_tcp_destroy for dev_udp ok.");
    }
    if(NULL != _tst) {
        ret = modbus_tcp_destroy(&_tst);
        if(MODBUS_RT_EOK != ret) {
            LOG_E("modbus_tcp_destroy for dev_tcp error,code:%d.", ret);
            return ret;
        }
        LOG_D("modbus_tcp_destroy for dev_tcp ok.");
    }
    rt_thread_mdelay(100);
    return ret;
}
