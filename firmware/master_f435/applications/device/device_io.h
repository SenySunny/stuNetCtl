/*
 * Copyright (c) 2006-2021, RT-Thread Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2023-09-09     SenyPC       the first version
 */
#ifndef __DEVICE_IO_H_
#define __DEVICE_IO_H_

#ifdef __cplusplus
 extern "C" {
#endif

#include "drv_common.h"
#include "drivers/pin.h"
#include "drivers/adc.h"
#include "drivers/dac.h"

#include "device_config.h"

extern rt_base_t _io_di_setting_nums;
extern rt_base_t _io_di_pin[DEV_INNER_DI_NUMS];

extern rt_base_t _io_do_setting_nums;
extern rt_base_t _io_do_pin[DEV_INNER_DO_NUMS];

extern uint8_t io_get_di(int pin);
extern uint8_t io_get_do(int pin);
extern void io_set_do(int pin, rt_base_t value);

extern int io_read_di(int start_addr, int nums, uint8_t *val);
extern int io_read_do(int start_addr, int nums, uint8_t *val);
extern int io_write_do(int start_addr, int nums, uint8_t *val);

#ifdef __cplusplus
}
#endif


#endif /* APPLICATIONS_DEVICE_DEVICE_IO_H_ */
