/*
 * Copyright (c) 2006-2021, RT-Thread Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2023-09-09     SenyPC       the first version
 */
#ifndef __DEVICE_DEVICE_STATUS_H_
#define _DEVICE_DEVICE_STATUS_H_

#ifdef __cplusplus
 extern "C" {
#endif

#include "drv_common.h"
#include "device_config.h"

#define DEVICE_STATUS_SF_ENABLE         (1)

 /**
  * @brief   扩展设备接口信息
  */
 struct device_status_type {
#if DEVICE_STATUS_SF_ENABLE
     device_pin_num  status_sf_pin;             /* 外置存储器检测指示信号  */
#endif
     device_pin_num  status_error_pin;          /* 错误指示信号  */
     device_pin_num  status_run_pin;            /* 程序运行指示型号。  */

     device_pin_num  mcu_run_pin;             /* 运行程序检测引脚  */
     device_pin_num  mcu_pd_pin;                /* 电源检测信号  */
 };
 typedef struct device_status_type * device_status_type_t;


#endif /* APPLICATIONS_DEVICE_DEVICE_STATUS_H_ */
