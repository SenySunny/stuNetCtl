/*
 * Copyright (c) 2006-2021, RT-Thread Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2023-09-28     SenyPC       the first version
 */
#ifndef __DEVICE_SLAVE_UART_H_
#define __DEVICE_SLAVE_UART_H_

#ifdef __cplusplus
 extern "C" {
#endif

#include "drv_common.h"
#include "modbus_rtu.h"

#define     SERIAL_UART_NAME                    "uart6"

#define     UART_BAUD_DEFAULT                   BAUD_RATE_115200
#define     UART_BYTESIZE_DEFAULT               8
#define     UART_PARITY_DEFAULT                 'N'
#define     UART_STOPBITS_DEFAULT               1

struct device_uart_type {
    rtu_modbus_device_t dev;
    const char *devname;
    int baudrate;
    int bytesize;
    char parity;
    int stopbits;

};
typedef struct device_uart_type * device_uart_type_t;

#ifdef __cplusplus
}
#endif

#endif /* APPLICATIONS_DEVICE_SLAVE_UART_H_ */
