/*
 * Copyright (c) 2006-2021, RT-Thread Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2023-09-09     SenyPC       the first version
 */
#ifndef __DEVICE_DEVICE_DATA_H_
#define __DEVICE_DEVICE_DATA_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "device_config.h"
#include "modbus_slave.h"

extern uint8_t dev_master_flag;

extern uint8_t dev_di_data[DEV_X_BIT_NUMS];          //默认di数据
extern uint8_t dev_var_di_data[DEV_X_BIT_NUMS];    //默认虚拟di数据

extern uint8_t dev_do_data[DEV_Y_BIT_NUMS];          //默认do数据

extern uint8_t dev_m_bit_data[DEV_M_BIT_NUMS];
extern uint8_t dev_m_input_bit_data[DEV_M_BIT_NUMS];

extern uint16_t dev_d_reg_data[DEV_D_REG_NUMS];
extern uint16_t dev_d_input_reg_data[DEV_D_REG_NUMS];

extern uint16_t dev_sp_d1_reg_data[DEV_SP_D1_REG_NUMS];              //默认添加特殊4x数据，掉电不丢失

extern uint16_t dev_sp_d1_input_reg_data[DEV_SP_D1_REG_NUMS];      //默认添加特殊4x数据，掉电不丢失





void dev_write_bit(uint8_t *data_addr, uint8_t data);
uint8_t dev_read_bit(uint8_t *data_addr);
int dev_write_bits(uint8_t *data_addr, int len, uint8_t *data);
int dev_read_bits(uint8_t *data_addr, int len, uint8_t *data);

void dev_write_reg(uint16_t *data_addr, uint16_t data);
uint16_t dev_read_reg(uint16_t *data_addr);
int dev_write_regs(uint16_t *data_addr, int len, uint16_t *data);
int dev_read_regs(uint16_t *data_addr, int len, uint16_t *data);
void dev_data2modbus_slave(agile_modbus_slave_util_t *util);


#ifdef __cplusplus
}
#endif


#endif /* APPLICATIONS_DEVICE_DEVICE_DATA_H_ */
