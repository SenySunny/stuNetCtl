/*
 * Copyright (c) 2006-2021, RT-Thread Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2023-09-28     SenyPC       the first version
 */
#ifndef __IO_EXTEND_H_
#define __IO_EXTEND_H_

#ifdef __cplusplus
 extern "C" {
#endif

#include "drv_common.h"
#include "device_config.h"
#include "device_extend.h"
#include "modbus_rtu.h"

int io_ext_data_init(device_extend_data_t data, int di_nums, int do_nums);

uint8_t io_get_ext_di(int pin);
uint8_t io_get_ext_do(int pin);
int io_set_ext_do(int pin, rt_base_t value);

int io_read_ext_di(int start_addr, int nums, uint8_t *val);
int io_read_ext_do(int start_addr, int nums, uint8_t *val);
int io_write_ext_do(int start_addr, int nums, uint8_t *val);

#ifdef __cplusplus
}
#endif

#endif /* APPLICATIONS_DEVICE_DEVICE_IO_EXTEND_H_ */
