/*
 * Copyright (c) 2006-2021, RT-Thread Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2023-09-28     SenyPC       the first version
 */
#ifndef __DEVICE_SLAVE_NET_H_
#define __DEVICE_SLAVE_NET_H_

#ifdef __cplusplus
 extern "C" {
#endif

#include "drv_common.h"
#include "modbus_tcp.h"

#define    NET_PORT_INIT_UDP    502
//#define    NET_PORT_INIT_UDP    105
#define    NET_PORT_INIT_TCP    502

int dev_slave_init( void );
int dev_slave_net_clear( void );

#ifdef __cplusplus
}
#endif

#endif /* APPLICATIONS_DEVICE_SLAVE_NET_H_ */
