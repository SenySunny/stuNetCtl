/*
 * Copyright (c) 2006-2021, RT-Thread Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2023-09-09     SenyPC       the first version
 */
#ifndef __DEVICE_DEVICE_CONFIG_H_
#define __DEVICE_DEVICE_CONFIG_H_

#ifdef __cplusplus
 extern "C" {
#endif

#include <rtthread.h>
#include "drv_common.h"

typedef rt_base_t  device_pin_num;

#define     dev_pin_mode                rt_pin_mode
#define     dev_pin_write               rt_pin_write
#define     dev_pin_read                rt_pin_read
#define     dev_delay                   rt_thread_mdelay

#define     dev_extend_malloc           rt_malloc
#define     dev_extend_calloc           rt_calloc
#define     dev_extend_free             rt_free


#define    DEV_NAME                     "stuNetCtl"

#define     DEV_INNER_DI_NUMS           (14)               //设备本机DI数量
#define     DEV_INNER_DO_NUMS           (10)               //设备本机DO数量

//设置dido的数量一般为8的倍数，未被硬件占用的dido则可以作为虚拟dido使用
#define     DEV_SETTING_DI_NUMS         (16)                 //设备设定的DI数量，有时候硬件只有14个di，但是为了占领2组di，实际上会设置有16个di。
#define     DEV_SETTING_DO_NUMS         (16)                 //设备设定的DO数量，有时候硬件只有10个do，但是为了占领2组do，实际上会设置有16个do。


#define     DEV_INNER_AI_NUMS           (4)               //设备本机AI数量
#define     DEV_INNER_AO_NUMS           (2)               //设备本机AO数量

#define     DEV_SETTING_AI_NUMS         (4)                 //设备本机AI数量
#define     DEV_SETTING_AO_NUMS         (2)                 //设备本机AO数量

/*0x和1x寄存器的地址*/
#define     DEV_M_BIT_ADDR          (0)         //M寄存器的地址
#define     DEV_SP_M1_BIT_ADDR      (8000)      //特殊M寄存器1的地址，掉电可保存
#define     DEV_SP_M2_BIT_ADDR      (8256)      //特殊M寄存器2的地址，掉电可保存
#define     DEV_T_BIT_ADDR          (9000)      //T寄存器，定时器
#define     DEV_C_BIT_ADDR          (9500)      //C寄存器，计数器
#define     DEV_X_BIT_ADDR          (10000)     //X寄存器，DI的值，0x和1x均有该值,在var虚拟仿真标志位关闭时写入无效，打开时可以写入
#define     DEV_Y_BIT_ADDR          (20000)     //Y寄存器，DO的值
#define     DEV_S_BIT_ADDR          (30000)     //S寄存器的值


/*3x和4x寄存器的地址*/
#define     DEV_D_REG_ADDR          (0)         //D寄存器的值
#define     DEV_SP_D1_REG_ADDR      (8000)      //特殊D寄存器1：断电不丢失
#define     DEV_SP_D2_REG_ADDR      (8256)      //特殊D寄存器2：断电不丢失
#define     DEV_T_REG_ADDR          (9000)      //T寄存器：定时器寄存器
#define     DEV_C_REG_ADDR          (9500)      //C寄存器：计数器寄存器
#define     DEV_DPOS_REG_ADDR       (10000)      //电机轴DPOS的值，轴虚拟坐标的位置
#define     DEV_MPOS_REG_ADDR       (11000)      //电机轴MPOS的值，轴测量坐标的位置
#define     DEV_VPSPEED_REG_ADDR    (12000)      //电机轴VPSPEED的值，轴当前运动速度
#define     DEV_AO_REG_ADDR         (13000)      //Aout寄存器的值
#define     DEV_AI_REG_ADDR         (14000)      //Ain寄存器的值，3x，4x均有该值，在var虚拟仿真标志位关闭时写入无效，打开时可以写入

 /*0x和1x寄存器的的数量*/
#define     DEV_M_BIT_NUMS          (4096)      //设备标准M寄存器的数量
#define     DEV_SP_M1_BIT_NUMS      (256)      //设备特殊M寄存器1的数量
#define     DEV_SP_M2_BIT_NUMS      (256)      //设备特殊M寄存器2的数量
#define     DEV_T_BIT_NUMS          (256)      //T寄存器的数量
#define     DEV_C_BIT_NUMS          (256)      //C寄存器的数量
#define     DEV_X_BIT_NUMS          (1024)      //X寄存器的数量
#define     DEV_Y_BIT_NUMS          (1024)      //Y寄存器的数量
#define     DEV_S_BIT_NUMS          (2048)      //S寄存器的数量

 /*3x和4x寄存器的数量*/
#define     DEV_D_REG_NUMS          (7999)      //D寄存器的数量
#define     DEV_SP_D1_REG_NUMS      (256)      //特殊D寄存器11的数量
#define     DEV_SP_D2_REG_NUMS      (256)      //特殊D寄存器2的数量
#define     DEV_T_REG_NUMS          (9000)      //T寄存器，定时器的值
#define     DEV_C_REG_NUMS          (9500)      //C寄存器，计数器的值
#define     DEV_DPOS_REG_NUMS       (199)      //电机轴DPOS的数量
#define     DEV_MPOS_REG_NUMS       (199)      //电机轴MPOS的数量
#define     DEV_VPSPEED_REG_NUMS    (199)      //电机轴VPSPEED的数量
#define     DEV_AO_REG_NUMS         (128)      //Aout寄存器的数量
#define     DEV_AI_REG_NUMS         (128)      //Ain寄存器的数量
 /**
  @verbatim
     设备错误信息
  @endverbatim
  */
 #define DEVICE_ERR_NO                          0                /**< 没有错误信息 */
 #define DEVICE_ERR_ENOMEM                       5                /**< 内存空间不足 */
 #define DEVICE_ERR_EXT                         100               /**< 外设检测错误 */
 #define DEVICE_ERR_POWER24                     101               /**< 未检测到电源正常输入 */

  /**
   @verbatim
      device 警告信息
   @endverbatim
   */
  #define DEVICE_WAR_NO                          0                /**< 没有错误信息 */



  struct device_err_type {
      int err_code;
      int err_id;

      int war_code;
      int war_id;
  };


#ifdef __cplusplus
}
#endif

#endif /* APPLICATIONS_DEVICE_DEVICE_CONFIG_H_ */
