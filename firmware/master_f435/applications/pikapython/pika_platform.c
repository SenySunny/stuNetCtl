/*
 * Copyright (c) 2006-2021, RT-Thread Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2021-12-07     lyon       the first version
 */

#include <rtthread.h>
#include "PikaPlatform.h"
#include "device_config.h"
#include <stdio.h>
#include <dirent.h>
#include <sys/stat.h>
#include <sys/types.h>
#include "unistd.h"
#include <fcntl.h>


//int __platform_vsprintf(char* buff, char* fmt, va_list args){
//    return rt_vsprintf(buff, fmt, args);
//}

void* __platform_malloc(size_t size) {
    return rt_malloc(size);
}

void* __platform_realloc(void* ptr, size_t size) {
    return rt_realloc(ptr, size);
}

void* __platform_calloc(size_t num, size_t size) {
    return rt_calloc(num, size);
}

void __platform_free(void* ptr) {
    rt_free(ptr);
}

int pika_platform_putchar(char ch) {
    return putchar(ch);
}

int pika_platform_vsnprintf(char* buff,
        size_t size,
        const char* fmt,
        va_list args) {
    return rt_vsnprintf(buff, size, fmt, args);
}


void* __platform_memset(void* mem, int ch, size_t size) {
    return rt_memset(mem, ch, size);
}
void* __platform_memcpy(void* dir, const void* src, size_t size) {
    return rt_memcpy(dir, src, size);
}

int pika_platform_memcmp(const void* s1, const void* s2, size_t n) {
    return rt_memcmp(s1, s2, n);
}

void* pika_platform_memmove(void* s1, void* s2, size_t n) {
    return rt_memmove(s1, s2, n);
}

char pika_platform_getchar(void) {
    char ch = 0;
    if(read(STDIN_FILENO, &ch, 1) > 0) {
        return ch;
    } else {
        return -1; /* EOF */
    }
    return -1;
}

FILE *pika_platform_fopen(const char *filename, const char *modes) {
  return fopen(filename, modes);
}

int pika_platform_fclose(FILE *fp) {
    return fclose(fp);
}

size_t pika_platform_fwrite(const void *ptr, size_t size, size_t count,
                            FILE *fp) {
  return fwrite(ptr, size, count, fp);
}

size_t pika_platform_fread(void *ptr, size_t size, size_t count, FILE *fp) {
  return fread(ptr, size, count, fp);
}

int pika_platform_fseek(FILE *fp, long offset, int whence) {
  return fseek(fp, offset, whence);
}

long pika_platform_ftell(FILE *fp) {
    return ftell(fp);
}

int pika_platform_remove(const char* pathname) {
    return remove(pathname);
}

int pika_platform_rename(const char* oldpath, const char* newpath) {
    return rename(oldpath, newpath);
}

char** pika_platform_listdir(const char* path, int* count) {
    struct dirent* dp;
    DIR* dir = opendir(path);

    char** filenames = NULL;
    *count = 0;

    if (dir != NULL) {
        while ((dp = readdir(dir)) != NULL) {
            if (strcmp(dp->d_name, ".") != 0 && strcmp(dp->d_name, "..") != 0) {
                size_t size_old = (*count) * sizeof(char*);
                size_t size_new = (size_old) + sizeof(char*);
                filenames =
                    (char**)pika_reallocn(filenames, size_old, size_new);
                filenames[*count] = pika_platform_strdup(dp->d_name);
                (*count)++;
            }
        }
        closedir(dir);
    }
    return filenames;
}


void pika_platform_reboot(void) {
    rt_hw_cpu_reset();
}


int gethostname(char *name, size_t len)
{
    int dev_name_len= strlen(DEV_NAME);
    if(len >= 16) {
        memcpy(name, DEV_NAME, dev_name_len);
        name[dev_name_len] = 0;
    }
    return dev_name_len;
}

//void pikaRTThread_Thread_mdelay(PikaObj *self, int ms){
//    rt_thread_mdelay(ms);
//}
//
//void pikaRTThread_Task_platformGetTick(PikaObj *self){
//    obj_setInt(self, "tick", rt_tick_get());
//}
